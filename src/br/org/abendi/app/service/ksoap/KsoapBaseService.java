package br.org.abendi.app.service.ksoap;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Context;
import br.org.abendi.app.util.Constants;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.google.gson.Gson;

public class KsoapBaseService extends BaseService {

	protected static final String NAMESPACE = "http://tempuri.org/";

	private String methodName = "";
	private String soapAction = "";
	
	public KsoapBaseService(KsoapServiceBuilder builder) {
		super(builder);
		this.methodName = builder.getMethodName();
		this.soapAction = builder.getSoapAction();
	}

	@Override
	public void refreshServiceBuilder(ServiceBuilder builder) {
		super.refreshServiceBuilder(builder);
		this.methodName = ((KsoapServiceBuilder)builder).getMethodName();
	}

	public Object callService(Context ctx) throws ParseException,
			ConnectionException, ConnectException {
		String data = null;

		SoapObject request = new SoapObject(NAMESPACE, methodName);
		
		if (params != null && params.size() > 0) {
			for (String key : params.keySet()) {
				List<Object> lst = params.get(key);
				if (lst != null && lst.size() > 0) {
					request.addProperty(key, lst.get(0).toString());
				}
			}
		}
		
		SoapSerializationEnvelope envelope = KsoapUtil.getSoapSerializationEnvelope(request);

		HttpTransportSE ht = KsoapUtil.getHttpTransportSE();
		try {
			ht.call(Constants.SOAP_ACTION + soapAction, envelope);
			KsoapUtil.testHttpResponse(ht);
			SoapPrimitive resultsString = (SoapPrimitive) envelope
					.getResponse();

			data = resultsString.toString();

			Gson gson = Util.getGson(null, jsonDeserializer, targetType);
			if (data != null) {
				if (String.class.equals(targetType.getType())) {
					return data;
				} else {
					return gson.fromJson(data, targetType.getType());
				}
			}
		} catch (SocketTimeoutException t) {
			t.printStackTrace();
		} catch (IOException i) {
			i.printStackTrace();
		} catch (Exception q) {
			q.printStackTrace();
		}
		return null;
	}
}
