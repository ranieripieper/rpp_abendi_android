package br.org.abendi.app.view.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import br.org.abendi.app.R;
import br.org.abendi.app.model.Profissional;
import br.org.abendi.app.service.ksoap.KsoapBaseService;
import br.org.abendi.app.service.ksoap.KsoapServiceBuilder;
import br.org.abendi.app.view.adapter.SearchResultAdapter;
import br.org.abendi.app.view.base.BaseActivity;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;

public class SearchResultActivity extends BaseActivity {

	private PagingListView lstView;
	private SearchResultAdapter adapter;
	private TextView txtQtdeResultados;
	private String valor;
	private String tipo;
	private static final String PARAM_TIPO = "PARAM_TIPO";
	private static final String PARAM_VALOR = "PARAM_VALOR";
	private static final String PARAM_SHOW_NENHUM_RESULT = "PARAM_SHOW_NENHUM_RESULT";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_result_activity);
		lstView = (PagingListView)findViewById(R.id.list_view);
		txtQtdeResultados = (TextView)findViewById(R.id.txt_qtde_resultados);
		
		if (getIntent() != null && getIntent().getExtras() != null && (getIntent().getExtras().getString(PARAM_TIPO) != null) || 
				getIntent().getExtras().getBoolean(PARAM_SHOW_NENHUM_RESULT)) {
			boolean showNenhumRes = getIntent().getExtras().getBoolean(PARAM_SHOW_NENHUM_RESULT, false);
			if (showNenhumRes) {
				showNenhumResultado();
			} else {
				tipo = getIntent().getExtras().getString(PARAM_TIPO);
				valor = getIntent().getExtras().getString(PARAM_VALOR);
				callService();
			}
		} else {
			finish();
		}
		
		lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Profissional prof = (Profissional) parent.getItemAtPosition(position);
				DetailProfissionalActivity.showActivity(getContext(), prof.getSnqc());
			}
		});
	}
	
	private void callService() {
		ObserverAsyncTask<Profissional[]> observer = new ObserverAsyncTask<Profissional[]>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(Profissional[] result) {
				if (result != null && result.length > 0) {
					List<Profissional> lst = new ArrayList<Profissional>();
					lst.addAll(Arrays.asList(result));
					populateView(lst);
				} else {
					showNenhumResultado();
				}
			}

			@Override
			public void onCancelled() {
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		KsoapServiceBuilder sb = new KsoapServiceBuilder();
		sb.setMethodName("SnqcConsultaPublicaJSON")
			.setSoapAction("SnqcConsultaPublicaJSON")
			.setObserverAsyncTask(observer).setNeedConnection(true)
			.addParams("_tipo", tipo)
			.addParams("_valor", valor);
	
		AsyncTaskService async = sb.mappingInto(this, Profissional[].class, new KsoapBaseService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
		
	}
	
	private void populateView(List<Profissional> profissionais) {
		dismissWaitDialog(true);
		if (profissionais == null || profissionais.isEmpty()) {
			showNenhumResultado();
		} else {
			findViewById(R.id.img_bg).setVisibility(View.GONE);
			if (adapter == null) {
				adapter = new SearchResultAdapter(getContext(), new ArrayList<Profissional>());
				lstView.setAdapter(adapter);
			}
			
			//if (Constants.TOTAL_ITENS_PAGE > profissionais.size()) {
				lstView.onFinishLoading(false, profissionais);
	    	//} else {
	    	//	lstView.onFinishLoading(true, profissionais);
	    	//}
			
			txtQtdeResultados.setText(getString(R.string.txt_qtde_registros, profissionais.size()));
		}
	}
	
	private void showNenhumResultado() {
		dismissWaitDialog(false);
		findViewById(R.id.img_bg).setVisibility(View.VISIBLE);
		findViewById(R.id.layout_nenhum_resultado).setVisibility(View.VISIBLE);
	}
	
	@Override
	protected int hideMenu() {
		return -1;
	}
	
	@Override
	protected boolean homeAsUpEnabled() {
		return false;
	}
	
	public static void showActivity(Context ctx, boolean showNenhumResult) {
		Intent it = new Intent(ctx, SearchResultActivity.class);
		it.putExtra(PARAM_SHOW_NENHUM_RESULT, showNenhumResult);
		ctx.startActivity(it);
	}
	
	public static void showActivity(Context ctx, String snqc, String nome, String tecnica) {
		Intent it = new Intent(ctx, SearchResultActivity.class);
		String tipo = "";
		String valor = "";
		if (!TextUtils.isEmpty(snqc)) {
			tipo = "snqc";
			valor = snqc;
		} else if (!TextUtils.isEmpty(nome)) {
			tipo = "nome";
			valor = nome;
		} else if (!TextUtils.isEmpty(tecnica)) {
			tipo = "tecnica";
			valor = tecnica;
		}
		it.putExtra(PARAM_TIPO, tipo);
		it.putExtra(PARAM_VALOR, valor);
		ctx.startActivity(it);
	}
	
	@Override
	protected void refresh() {
		super.refresh();
		callService();
	}
}
