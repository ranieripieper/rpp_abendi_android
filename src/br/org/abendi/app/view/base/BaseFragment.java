package br.org.abendi.app.view.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import br.org.abendi.app.R;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.listeners.ActionClickListener;

public class BaseFragment extends Fragment {

	private ProgressDialog progress;
	protected LoadingView loadingView;
	protected TouchBlackHoleView black_hole;
	protected Snackbar mSnackBar;
	protected AsyncTaskService async;
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (getView() != null) { 
			loadingView = (LoadingView)getView().findViewById(R.id.progress_bar);
			black_hole = (TouchBlackHoleView)getView().findViewById(R.id.black_hole);
		}
	}
	
	public void showWaitDialog(boolean blockScreen) {
		if (black_hole != null) {
			if (blockScreen) {
				black_hole.disable_touch(true);
				black_hole.setVisibility(View.VISIBLE);
			} else {
				black_hole.disable_touch(false);
				black_hole.setVisibility(View.GONE);
			}
		}
		
		if (loadingView != null) {
			loadingView.setVisibility(View.VISIBLE);
		} else {
			progress = ProgressDialog.show(getActivity(), getString(R.string.title_aguarde),
					getString(R.string.msg_aguarde), true);
			progress.show();
		}
		hideContent();
	}
	
	protected void showContent() {
		if (getView() != null && getView().findViewById(R.id.content) != null) {
			getView().findViewById(R.id.content).setVisibility(View.VISIBLE);
		}
	}
	
	protected void hideContent() {
		if (getView() != null && getView().findViewById(R.id.content) != null) {
			getView().findViewById(R.id.content).setVisibility(View.GONE);
		}
	}
	
	
	public void showWaitDialog() {
		showWaitDialog(true);
	}
	
	public void dismissWaitDialog(boolean showContent) {
		if (black_hole != null) {
			black_hole.disable_touch(false);
			black_hole.setVisibility(View.GONE);
		}
		if (loadingView != null) {
			loadingView.setVisibility(View.GONE);
		}
		if (progress != null) {
			progress.dismiss();
		}
		if (showContent) {
			showContent();
		}
	}
	
	public void showError(Exception e, boolean tryAgain) {
		dismissWaitDialog(false);
		if (e != null) {
			int resMsg = R.string.msg_ocorreu_erro;
			if (e instanceof ConnectionNotFoundException) {
				resMsg = R.string.msg_sem_conexao;
			}
			if (tryAgain) {
				showSnack(resMsg);
			} else {
				showSimpleSnack(getString(resMsg));
			}
		} else {
			showSnack(R.string.msg_ocorreu_erro);
		}
	}
	
	public void showError(Exception e) {
		showError(e, true);
		
	}
	
	protected void showSimpleSnack(String msg) {
		if (mSnackBar != null) {
			mSnackBar.dismiss();
		}
		mSnackBar = Snackbar.with(getActivity())
                .text(msg)
                .duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
    	 SnackbarManager.show(mSnackBar);
	}
	
    protected void showSnack(int msg) {
    	ActionClickListener actionClickListener = new ActionClickListener() {
			
			@Override
			public void onActionClicked(Snackbar snackbar) {
				refresh();
			}
		};
    	
    	mSnackBar = Snackbar.with(getActivity())
                .text(msg)
                .actionLabel(R.string.txt_tentar_novemante)
                .actionListener(actionClickListener)
                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE);
    	 SnackbarManager.show(mSnackBar);
    }
    
    public void refresh() { 
    	if (mSnackBar != null) {
    		mSnackBar.dismiss();
    	}    	
    }
    
    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
        	show();
        } else {
        	stop();
        }
    }
    
    protected void stop() {
    	if (async != null) {
    		async.cancel(true);
    	}
    }
    
    protected void show() {}
	
	public void lockScreen() {
		if (black_hole != null) {
			black_hole.setVisibility(View.VISIBLE);
		}
	}
	
	public void unlockScreen() {
		if (black_hole != null) {
			black_hole.setVisibility(View.GONE);
		}
	}
}
