package br.org.abendi.app.model;

import java.util.Date;

import br.org.abendi.app.service.ksoap.KsoapUtil;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.google.gson.annotations.SerializedName;

public class Pagamento {

	@SerializedName("data_vencimento")
	public String dtVencimento;
	@SerializedName("valor_docto")
	public String valor;
	@SerializedName("historico")
	public String descricao;
	public String urlPgto;
	
	/**
	 * @return the dtVencimento
	 */
	public String getDtVencimentoFormat() {
		if (dtVencimento != null) {
			Date dt = KsoapUtil.formatDate(dtVencimento);
			if (dt != null) {
				return DateUtil.dfDiaMesAno.get().format(dt);
			}			
		}
		return "";
	}
	
	/**
	 * @return the dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}
	/**
	 * @param dtVencimento the dtVencimento to set
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the urlPgto
	 */
	public String getUrlPgto() {
		return urlPgto;
	}
	/**
	 * @param urlPgto the urlPgto to set
	 */
	public void setUrlPgto(String urlPgto) {
		this.urlPgto = urlPgto;
	}
	
	
	
}
