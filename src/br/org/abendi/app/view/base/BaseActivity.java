package br.org.abendi.app.view.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import br.org.abendi.app.R;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.listeners.ActionClickListener;

public abstract class BaseActivity extends ActionBarActivity {

	private ProgressDialog progress;
	protected LoadingView loadingView;
	protected TouchBlackHoleView black_hole;
	protected Snackbar mSnackBar;

	protected AsyncTaskService async;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		configActionBar();
	}
	
    private void configActionBar() {
    	getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setLogo(null);
        int titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        if (titleId > 0) {
        	TextView titleTextView = (TextView) findViewById(titleId);
            if (titleTextView != null) {
            	FontUtilCache.setCustomFont(BaseActivity.this, titleTextView, "fonts/Arial_Bold.ttf");
            }
        }
    }
    
	protected abstract boolean homeAsUpEnabled();
	
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		
		if (homeAsUpEnabled()) {
			
			getSupportActionBar().setDisplayHomeAsUpEnabled(false);
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			getSupportActionBar().setDisplayUseLogoEnabled(false);
			
		}		
		
		loadingView = (LoadingView)findViewById(R.id.progress_bar);
		black_hole = (TouchBlackHoleView)findViewById(R.id.black_hole);
		
	}
	
	protected boolean hiddenBackButton() {
		return true;
	}
	
	public void showWaitDialog(boolean blockScreen) {
		if (black_hole != null) {
			if (blockScreen) {
				black_hole.disable_touch(true);
				black_hole.setVisibility(View.VISIBLE);
			} else {
				black_hole.disable_touch(false);
				black_hole.setVisibility(View.GONE);
			}
		}
		
		if (loadingView != null) {
			loadingView.setVisibility(View.VISIBLE);
			loadingView.bringToFront();
		} else {
			progress = ProgressDialog.show(this, getString(R.string.title_aguarde),
					getString(R.string.msg_aguarde), true);
			progress.show();
		}
		if (findViewById(R.id.content) != null) {
			findViewById(R.id.content).setVisibility(View.GONE);
		}
	}
	
	public void showWaitDialog() {
		showWaitDialog(true);
	}
	
	public void dismissWaitDialog(boolean showContent) {
		if (black_hole != null) {
			black_hole.disable_touch(false);
			black_hole.setVisibility(View.GONE);
		}
		if (loadingView != null) {
			loadingView.setVisibility(View.GONE);
		}
		if (progress != null) {
			progress.dismiss();
		}
		
		if (showContent && findViewById(R.id.content) != null) {
			findViewById(R.id.content).setVisibility(View.VISIBLE);
		}
	}
	
	public void showError(Exception e) {
		showError(e, false);
		
	}
	
	public void showError(Exception e, boolean simpleSnack) {
		dismissWaitDialog(false);
		if (e != null) {
			int resMsg = R.string.msg_ocorreu_erro;
			if (e instanceof ConnectionNotFoundException) {
				resMsg = R.string.msg_sem_conexao;
			}
			if (simpleSnack) {
				showSimpleSnack(resMsg);
			} else {
				showSnack(resMsg);
			}
	        
		} else {
			if (simpleSnack) {
				showSimpleSnack(R.string.msg_ocorreu_erro);
			} else {
				showSnack(R.string.msg_ocorreu_erro);
			}
		}
		
	}
	
    protected void showSnack(int msg) {
    	ActionClickListener actionClickListener = new ActionClickListener() {
			
			@Override
			public void onActionClicked(Snackbar snackbar) {
				refresh();
			}
		};
    	
    	mSnackBar = Snackbar.with(BaseActivity.this)
                .text(msg)
                .actionLabel(R.string.txt_tentar_novemante)
                .actionListener(actionClickListener)
                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE);
    	 SnackbarManager.show(mSnackBar);
    }
    
    protected void showSimpleSnack(int msg) {
    	showSimpleSnack(msg, Snackbar.SnackbarDuration.LENGTH_SHORT);
    }
    
    protected void showSimpleSnack(int msg, Snackbar.SnackbarDuration duration) {
    	mSnackBar = Snackbar.with(BaseActivity.this)
                .text(msg)
                .duration(duration);
    	 SnackbarManager.show(mSnackBar);
    }
      
    protected void refresh() { 
    	if (mSnackBar != null) {
    		mSnackBar.dismiss();
    	} 
    	SnackbarManager.dismiss();
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }
    

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
    }
	
	protected void gpsEnable(Location location) {
		
	}
	
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
		if (hideMenu() != -1) {
			menu.findItem(hideMenu()).setVisible(false);
		}
		return super.onCreateOptionsMenu(menu);
   }
   
   @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
   
	protected abstract int hideMenu();

	protected Context getContext() {
		return BaseActivity.this;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (async != null) {
			async.cancel(true);
		}
	}
}
