package br.org.abendi.app.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import br.org.abendi.app.R;
import br.org.abendi.app.view.adapter.PageAdapter;
import br.org.abendi.app.view.base.BaseActivity;


public class MainActivity extends BaseActivity {

	private Fragment actualFragment;
	
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private PageAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    public static final int MENU_AREA_RESTRITA = 0;
    public static final int MENU_BUSCA = 1;
    public static final int MENU_SOBRE = 2;
    
    private LinearLayout layoutMenuAreaRestrita;
    private LinearLayout layoutMenuBusca;
    private LinearLayout layoutMenuSobre;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layoutMenuAreaRestrita = (LinearLayout)findViewById(R.id.menu_area_restrita);
        layoutMenuBusca = (LinearLayout)findViewById(R.id.menu_busca_publica);
        layoutMenuSobre = (LinearLayout)findViewById(R.id.menu_sobre);
        
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new PageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        
        mViewPager.setCurrentItem(MENU_AREA_RESTRITA);
        selectPage(MENU_AREA_RESTRITA);
        
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int page) {
				selectPage(page);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
        
        layoutMenuAreaRestrita.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mViewPager.setCurrentItem(MENU_AREA_RESTRITA);
			}
		});
        layoutMenuBusca.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mViewPager.setCurrentItem(MENU_BUSCA);
			}
		});
        layoutMenuSobre.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mViewPager.setCurrentItem(MENU_SOBRE);
			}
		});
    }    
    
    private void selectPage(int page) {
    	if (page == MENU_AREA_RESTRITA) {
    		layoutMenuAreaRestrita.setBackgroundResource(R.color.select_menu_bar);
    		layoutMenuBusca.setBackgroundResource(R.color.bg_menu_bar);
    		layoutMenuSobre.setBackgroundResource(R.color.bg_menu_bar);
    	} else if (page == MENU_BUSCA) {
    		layoutMenuAreaRestrita.setBackgroundResource(R.color.bg_menu_bar);
    		layoutMenuBusca.setBackgroundResource(R.color.select_menu_bar);
    		layoutMenuSobre.setBackgroundResource(R.color.bg_menu_bar);
    	} else if (page == MENU_SOBRE) {
    		layoutMenuAreaRestrita.setBackgroundResource(R.color.bg_menu_bar);
    		layoutMenuBusca.setBackgroundResource(R.color.bg_menu_bar);
    		layoutMenuSobre.setBackgroundResource(R.color.select_menu_bar);
    	}
    	
    	actualFragment = mSectionsPagerAdapter.getItem(page);
    }
    
    @Override
    protected int hideMenu() {
    	return -1;
    }
    @Override
    protected boolean homeAsUpEnabled() {
    	return false;
    }

}
