package br.org.abendi.app.model;

import com.google.gson.annotations.SerializedName;

public class LoginResult {

	@SerializedName("codigo_snqc")
	private String snqc;
	
	@SerializedName("codigo_socio")
	private String codigoSocio;
	
	@SerializedName("nome")
	private String nome;
	
	@SerializedName("chave_seguranca")
	private String chaveSeguranca;

	/**
	 * @return the snqc
	 */
	public String getSnqc() {
		return snqc;
	}

	/**
	 * @param snqc the snqc to set
	 */
	public void setSnqc(String snqc) {
		this.snqc = snqc;
	}

	/**
	 * @return the codigoSocio
	 */
	public String getCodigoSocio() {
		return codigoSocio;
	}

	/**
	 * @param codigoSocio the codigoSocio to set
	 */
	public void setCodigoSocio(String codigoSocio) {
		this.codigoSocio = codigoSocio;
	}

	/**
	 * @return the chaveSeguranca
	 */
	public String getChaveSeguranca() {
		return chaveSeguranca;
	}

	/**
	 * @param chaveSeguranca the chaveSeguranca to set
	 */
	public void setChaveSeguranca(String chaveSeguranca) {
		this.chaveSeguranca = chaveSeguranca;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
}
