package br.org.abendi.app.view.activity;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import br.org.abendi.app.R;
import br.org.abendi.app.model.NotaExame;
import br.org.abendi.app.model.NotaExameResult;
import br.org.abendi.app.service.ksoap.KsoapServiceBuilder;
import br.org.abendi.app.service.ksoap.NotaExameService;
import br.org.abendi.app.util.Preferences;
import br.org.abendi.app.view.base.BaseActivity;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;

public class NotaActivity extends BaseActivity {

	private TextView txtTodos;
	private TextView txtTeorico;
	private TextView txtPratico;
	private TextView txtRecertificacao;
	private ImageView imgArrow;
	private float lastPosArrow = 0;
	private LayoutInflater inflater;
	private LinearLayout exTeoricos;
	private LinearLayout exPraticos;
	private LinearLayout exRecertificacao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nota_activity);
		initComponents();
		
		callService();
	}
	
	private void initComponents() {
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		txtTodos = (TextView)findViewById(R.id.txt_todos);
		txtTeorico = (TextView)findViewById(R.id.txt_teorico);
		txtPratico = (TextView)findViewById(R.id.txt_pratico);
		txtRecertificacao = (TextView)findViewById(R.id.txt_recertificacao);
		
		exTeoricos = (LinearLayout)findViewById(R.id.layout_exame_teorico);
		exPraticos =  (LinearLayout)findViewById(R.id.layout_exame_pratico);
		exRecertificacao =  (LinearLayout)findViewById(R.id.layout_recertificacao);
		
		imgArrow = (ImageView)findViewById(R.id.img_arrow);
		
		txtTodos.setOnClickListener(clickTxtHeader);
		txtTodos.setTag("txtTodos");
		txtTeorico.setOnClickListener(clickTxtHeader);
		txtTeorico.setTag("txtTeorico");
		txtPratico.setOnClickListener(clickTxtHeader);
		txtPratico.setTag("txtPratico");
		txtRecertificacao.setOnClickListener(clickTxtHeader);
		txtRecertificacao.setTag("txtRecertificacao");
		
		txtTodos.postDelayed(new Runnable() {
			@Override
			public void run() {
				moveArrow(txtTodos);
			}
		}, 1);
	}
	
	View.OnClickListener clickTxtHeader = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			moveArrow(v);
			if ("txtTodos".equals(v.getTag())) {
				exTeoricos.setVisibility(View.VISIBLE);
				exPraticos.setVisibility(View.VISIBLE);
				exRecertificacao.setVisibility(View.VISIBLE);
			} else if("txtTeorico".equals(v.getTag())) {
				exTeoricos.setVisibility(View.VISIBLE);
				exPraticos.setVisibility(View.GONE);
				exRecertificacao.setVisibility(View.GONE);
			} else if("txtPratico".equals(v.getTag())) {
				exTeoricos.setVisibility(View.GONE);
				exPraticos.setVisibility(View.VISIBLE);
				exRecertificacao.setVisibility(View.GONE);
			} else if("txtRecertificacao".equals(v.getTag())) {
				exTeoricos.setVisibility(View.GONE);
				exPraticos.setVisibility(View.GONE);
				exRecertificacao.setVisibility(View.VISIBLE);
			}
			
			((ScrollView)findViewById(R.id.scroll_view)).scrollTo(0, 0);
		}
	};
	
	private void moveArrow(View v) {
		
		FontUtilCache.setCustomFont(getContext(), txtPratico, "fonts/Arial.ttf");
    	FontUtilCache.setCustomFont(getContext(), txtRecertificacao, "fonts/Arial.ttf");
    	FontUtilCache.setCustomFont(getContext(), txtTeorico, "fonts/Arial.ttf");
    	FontUtilCache.setCustomFont(getContext(), txtTodos, "fonts/Arial.ttf");
    	
		v.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		imgArrow.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float nexPos = v.getLeft() + v.getMeasuredWidth()/2 - imgArrow.getMeasuredWidth()/2;
    	TranslateAnimation animation3 = new TranslateAnimation(lastPosArrow, nexPos, 0, 0);
    	animation3.setDuration(500);
    	animation3.setFillAfter(true);
    	imgArrow.startAnimation(animation3);
    	lastPosArrow = nexPos;
    	
    	if (v instanceof TextView) {
    		FontUtilCache.setCustomFont(getContext(), (TextView)v, "fonts/Arial_Bold.ttf"); 
    	}
    	
	}
	
	private void callService() {
		ObserverAsyncTask<NotaExameResult> observer = new ObserverAsyncTask<NotaExameResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(NotaExameResult result) {
				if (result != null) {
					populateView(result);
					dismissWaitDialog(true);
				} else {
					showNenhumResultado();
				}
			}

			@Override
			public void onCancelled() {
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		KsoapServiceBuilder sb = new KsoapServiceBuilder();
			sb.setObserverAsyncTask(observer).setNeedConnection(true)
			.addParams("numero_snqc", Preferences.getSnqc(getContext()));
	
		AsyncTaskService async = sb.mappingInto(this, NotaExameResult.class, new NotaExameService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void populateView(NotaExameResult result) {
		
		if (result.getExTeoricos() != null && !result.getExTeoricos().isEmpty()) {
			configRowExTeorico(exTeoricos, result.getExTeoricos());
		} else {
			findViewById(R.id.layout_nenhum_resultado_ex_teorico).setVisibility(View.VISIBLE);
		}
		if (result.getExPraticos() != null && !result.getExPraticos().isEmpty()) {
			configRowExPratico(exPraticos, result.getExPraticos());
		} else {
			findViewById(R.id.layout_nenhum_resultado_ex_pratico).setVisibility(View.VISIBLE);
		}
		if (result.getExRecertificacao() != null && !result.getExRecertificacao().isEmpty()) {
			configRowExRecertificacao(exRecertificacao, result.getExRecertificacao());
		} else {
			findViewById(R.id.layout_nenhum_resultado_ex_recertificacao).setVisibility(View.VISIBLE);
		}		
		
	}
	
	private void configRowExTeorico(LinearLayout v, List<NotaExame> lstNotaExame) {
		if (lstNotaExame != null && !lstNotaExame.isEmpty()) {
			
			for (int i=0; i < lstNotaExame.size(); i++) {
				NotaExame agEx = lstNotaExame.get(i);
				View vInter = inflater.inflate(R.layout.nota_activity_ex_teorico_row, null);
				TextView txtData = (TextView)vInter.findViewById(R.id.txt_data);
				TextView txtTecnica = (TextView)vInter.findViewById(R.id.txt_tecnica);
				TextView txtNota1 = (TextView)vInter.findViewById(R.id.txt_nota1);
				TextView txtNota2 = (TextView)vInter.findViewById(R.id.txt_nota2);
				
				TextView txtResultado = (TextView)vInter.findViewById(R.id.txt_resultado);
				
				txtData.setText(agEx.getDataFormat());
				txtTecnica.setText(agEx.getTecnica());

				txtNota1.setText(agEx.getNotaEspecifica());
				txtNota2.setText(agEx.getNotaGeral());
				
				txtResultado.setText(agEx.getResultado());
				
				v.addView(vInter);
			}
		}
	}
	
	private void configRowExRecertificacao(LinearLayout v, List<NotaExame> lstNotaExame) {
		if (lstNotaExame != null && !lstNotaExame.isEmpty()) {
			
			for (int i=0; i < lstNotaExame.size(); i++) {
				NotaExame agEx = lstNotaExame.get(i);
				View vInter = inflater.inflate(R.layout.nota_activity_ex_recertificacao_row, null);
				TextView txtData = (TextView)vInter.findViewById(R.id.txt_data);
				TextView txtTecnica = (TextView)vInter.findViewById(R.id.txt_tecnica);
				TextView txtNota1 = (TextView)vInter.findViewById(R.id.txt_nota1);
				TextView txtNota2 = (TextView)vInter.findViewById(R.id.txt_nota2);
				
				TextView txtResultado = (TextView)vInter.findViewById(R.id.txt_resultado);
				
				txtData.setText(agEx.getDataFormat());
				txtTecnica.setText(agEx.getTecnica());

				txtNota1.setText(agEx.getNota1Etapa1());
				txtNota2.setText(agEx.getNota2Etapa1());
				
				txtResultado.setText(agEx.getResultado());
				
				v.addView(vInter);
			}
		}
	}
	
	private void configRowExPratico(LinearLayout v, List<NotaExame> lstNotaExame) {
		if (lstNotaExame != null && !lstNotaExame.isEmpty()) {
			
			for (int i=0; i < lstNotaExame.size(); i++) {
				NotaExame agEx = lstNotaExame.get(i);
				View vInter = inflater.inflate(R.layout.nota_activity_ex_pratico_row, null);
				TextView txtData = (TextView)vInter.findViewById(R.id.txt_data);
				TextView txtTecnica = (TextView)vInter.findViewById(R.id.txt_tecnica);
				TextView txtNota1 = (TextView)vInter.findViewById(R.id.txt_nota1);
				TextView txtNota2 = (TextView)vInter.findViewById(R.id.txt_nota2);
				TextView txtNota3 = (TextView)vInter.findViewById(R.id.txt_nota3);
				TextView txtMedia1 = (TextView)vInter.findViewById(R.id.txt_media1);
				TextView txtMedia2 = (TextView)vInter.findViewById(R.id.txt_media2);
				TextView txtMedia3 = (TextView)vInter.findViewById(R.id.txt_media3);
				TextView txtResultado = (TextView)vInter.findViewById(R.id.txt_resultado);
				
				txtData.setText(agEx.getDataFormat());
				txtTecnica.setText(agEx.getTecnica());

				txtNota1.setText(agEx.getNota1Etapa1());
				txtNota2.setText(agEx.getNota1Etapa2());
				txtNota3.setText(agEx.getNota1Etapa3());
				
				txtMedia1.setText(agEx.getMediaEtapa1());
				txtMedia2.setText(agEx.getMediaEtapa2());
				txtMedia3.setText(agEx.getMediaEtapa3());
				
				txtResultado.setText(agEx.getResultado());
				
				v.addView(vInter);
			}
		}
	}
	
	private void showNenhumResultado() {
		dismissWaitDialog(false);
		findViewById(R.id.layout_nenhum_resultado).setVisibility(View.VISIBLE);
	}
	
	public static void showActivity(Context ctx) {
		Intent it = new Intent(ctx, NotaActivity.class);
		ctx.startActivity(it);
	}
	
	@Override
	protected int hideMenu() {
		return -1;
	}
	
	@Override
	protected boolean homeAsUpEnabled() {
		return false;
	}
}
