package br.org.abendi.app.model;

import java.util.Date;

import android.text.TextUtils;
import br.org.abendi.app.service.ksoap.KsoapUtil;
import br.org.abendi.app.util.AbendiUtil;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.google.gson.annotations.SerializedName;

public class NotaExame {
	private String ceq;
	@SerializedName("codigo_tecnica")
	private String tecnica;
	private String data;
	private String hora;
	private String id;
	private String local;
	@SerializedName("media_etapa1")
	private String mediaEtapa1;
	@SerializedName("media_etapa2")
	private String mediaEtapa2;
	@SerializedName("media_etapa3")
	private String mediaEtapa3;
	
	@SerializedName("nota1_etapa1")
	private String nota1Etapa1;
	@SerializedName("nota1_etapa2")
	private String nota1Etapa2;
	@SerializedName("nota1_etapa3")
	private String nota1Etapa3;
	
	@SerializedName("nota2_etapa1")
	private String nota2Etapa1;
	@SerializedName("nota2_etapa2")
	private String nota2Etapa2;
	@SerializedName("nota2_etapa3")
	private String nota2Etapa3;
	
	@SerializedName("nota_especifica")
	private String notaEspecifica;
	@SerializedName("nota_geral")
	private String notaGeral;
	@SerializedName("numero_snqc")
	private String snqc;
	private String resultado;
/*
[
    {
        "ceq": "SENAI RJ",
        "codigo_tecnica": "LP-N2-G",
        "data": "/Date(1401678000000-0300)/",
        "hora": "",
        "id": 13741,
        "local": "Rua Ṣo Francisco Xavier, 601 - Maracaṇ",
        "media_etapa1": "",
        "media_etapa2": "",
        "media_etapa3": "",
        "nota1_etapa1": "8.6",
        "nota1_etapa2": "",
        "nota1_etapa3": "",
        "nota2_etapa1": "8.6",
        "nota2_etapa2": "",
        "nota2_etapa3": "",
        "nota_especifica": "",
        "nota_geral": "",
        "numero_snqc": 4345,
        "resultado": "APROVADO"
    }
]
 */
	/**
	 * @return the ceq
	 */
	public String getCeq() {
		return ceq;
	}
	/**
	 * @param ceq the ceq to set
	 */
	public void setCeq(String ceq) {
		this.ceq = ceq;
	}
	/**
	 * @return the tecnica
	 */
	public String getTecnica() {
		return tecnica;
	}
	/**
	 * @param tecnica the tecnica to set
	 */
	public void setTecnica(String tecnica) {
		this.tecnica = tecnica;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	/**
	 * @return the data
	 */
	public String getDataFormat() {
		if (data != null) {
			Date dt = KsoapUtil.formatDate(data);
			if (dt != null) {
				return DateUtil.dfDiaMesAno.get().format(dt);
			}			
		}
		return "";		
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}
	/**
	 * @return the hora
	 */
	public String getHora() {
		return hora;
	}
	/**
	 * @param hora the hora to set
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the local
	 */
	public String getLocal() {
		return AbendiUtil.getLocal(local);
	}
	/**
	 * @param local the local to set
	 */
	public void setLocal(String local) {
		this.local = local;
	}
	/**
	 * @return the mediaEtapa1
	 */
	public String getMediaEtapa1() {
		return getNotaFormat(mediaEtapa1);
	}
	/**
	 * @param mediaEtapa1 the mediaEtapa1 to set
	 */
	public void setMediaEtapa1(String mediaEtapa1) {
		this.mediaEtapa1 = mediaEtapa1;
	}
	/**
	 * @return the mediaEtapa2
	 */
	public String getMediaEtapa2() {
		return getNotaFormat(mediaEtapa2);
	}
	/**
	 * @param mediaEtapa2 the mediaEtapa2 to set
	 */
	public void setMediaEtapa2(String mediaEtapa2) {
		this.mediaEtapa2 = mediaEtapa2;
	}
	/**
	 * @return the mediaEtapa3
	 */
	public String getMediaEtapa3() {
		return getNotaFormat(mediaEtapa3);
	}
	/**
	 * @param mediaEtapa3 the mediaEtapa3 to set
	 */
	public void setMediaEtapa3(String mediaEtapa3) {
		this.mediaEtapa3 = mediaEtapa3;
	}
	/**
	 * @return the nota1Etapa1
	 */
	public String getNota1Etapa1() {
		return getNotaFormat(nota1Etapa1);
	}
	/**
	 * @param nota1Etapa1 the nota1Etapa1 to set
	 */
	public void setNota1Etapa1(String nota1Etapa1) {
		this.nota1Etapa1 = nota1Etapa1;
	}
	/**
	 * @return the nota1Etapa2
	 */
	public String getNota1Etapa2() {
		return getNotaFormat(nota1Etapa2);
	}
	/**
	 * @param nota1Etapa2 the nota1Etapa2 to set
	 */
	public void setNota1Etapa2(String nota1Etapa2) {
		this.nota1Etapa2 = nota1Etapa2;
	}
	/**
	 * @return the nota1Etapa3
	 */
	public String getNota1Etapa3() {
		return getNotaFormat(nota1Etapa3);
	}
	/**
	 * @param nota1Etapa3 the nota1Etapa3 to set
	 */
	public void setNota1Etapa3(String nota1Etapa3) {
		this.nota1Etapa3 = nota1Etapa3;
	}
	/**
	 * @return the nota2Etapa1
	 */
	public String getNota2Etapa1() {
		return getNotaFormat(nota2Etapa1);
	}
	/**
	 * @param nota2Etapa1 the nota2Etapa1 to set
	 */
	public void setNota2Etapa1(String nota2Etapa1) {
		this.nota2Etapa1 = nota2Etapa1;
	}
	/**
	 * @return the nota2Etapa2
	 */
	public String getNota2Etapa2() {
		return getNotaFormat(nota2Etapa2);
	}
	/**
	 * @param nota2Etapa2 the nota2Etapa2 to set
	 */
	public void setNota2Etapa2(String nota2Etapa2) {
		this.nota2Etapa2 = nota2Etapa2;
	}
	/**
	 * @return the nota2Etapa3
	 */
	public String getNota2Etapa3() {
		return getNotaFormat(nota2Etapa3);
	}
	/**
	 * @param nota2Etapa3 the nota2Etapa3 to set
	 */
	public void setNota2Etapa3(String nota2Etapa3) {
		this.nota2Etapa3 = nota2Etapa3;
	}
	/**
	 * @return the notaEspecifica
	 */
	public String getNotaEspecifica() {
		return getNotaFormat(notaEspecifica);
	}
	/**
	 * @param notaEspecifica the notaEspecifica to set
	 */
	public void setNotaEspecifica(String notaEspecifica) {
		this.notaEspecifica = notaEspecifica;
	}
	/**
	 * @return the notaGeral
	 */
	public String getNotaGeral() {
		return getNotaFormat(notaGeral);
	}
	/**
	 * @param notaGeral the notaGeral to set
	 */
	public void setNotaGeral(String notaGeral) {
		this.notaGeral = notaGeral;
	}
	/**
	 * @return the snqc
	 */
	public String getSnqc() {
		return snqc;
	}
	/**
	 * @param snqc the snqc to set
	 */
	public void setSnqc(String snqc) {
		this.snqc = snqc;
	}
	/**
	 * @return the resultado
	 */
	public String getResultado() {
		return resultado;
	}
	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
	private String getNotaFormat(String value) {
		if (TextUtils.isEmpty(value)) {
			return "-";
		}
		return value;
	}
}
