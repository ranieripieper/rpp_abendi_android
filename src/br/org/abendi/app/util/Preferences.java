package br.org.abendi.app.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

	private static final String PREFERENCES = Preferences.class + "";
	private static final String PREF_SNQC = "PREF_SNQC";
	private static final String PREF_SOCIO = "PREF_SOCIO";
	private static final String PREF_USER = "PREF_USER";
	private static final String PREF_PASS = "PREF_PASS";
	private static final String PREF_NAME = "PREF_NAME";
	private static final String PREF_TECNICAS = "PREF_TECNICAS";
	private static final String PREF_TECNICAS_LAST_UPDATE = "PREF_TECNICAS_LAST_UPDATE";
	
	public static ArrayList<String> getTecnicas(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		Set<String> set = settings.getStringSet(PREF_TECNICAS, null);
		if (set == null) {
			return null;
		} else {
			return new ArrayList<String>(set);
		}
	}
	
	public static void setTecnicas(Context ctx, List<String> value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putStringSet(PREF_TECNICAS, new HashSet<String>(value));
		editor.commit();
	}
	
	public static Date getTecnicasLastUpdate(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		long dt = settings.getLong(PREF_TECNICAS_LAST_UPDATE, -1);
		if (dt > 0) {
			return new Date(dt);
		}
		return null;
	}
	
	public static void setTecnicasLastUpdate(Context ctx, Date value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(PREF_TECNICAS_LAST_UPDATE, value.getTime());
		editor.commit();
	}
	
	public static String getUser(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		return settings.getString(PREF_USER, null);
	}
	
	public static void setUser(Context ctx, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_USER, value);
		editor.commit();
	}
	
	public static String getName(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		return settings.getString(PREF_NAME, null);
	}
	
	public static void setName(Context ctx, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_NAME, value);
		editor.commit();
	}
	
	public static String getPassword(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		return settings.getString(PREF_PASS, null);
	}
	
	public static void setPassword(Context ctx, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_PASS, value);
		editor.commit();
	}
	public static String getSnqc(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		return settings.getString(PREF_SNQC, null);
	}
	
	public static void setSnqc(Context ctx, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_SNQC, value);
		editor.commit();
	}
	
	public static String getCodigoSocio(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		return settings.getString(PREF_SOCIO, null);
	}
	
	public static void setCodigoSocio(Context ctx, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_SOCIO, value);
		editor.commit();
	}
	
	public static void logout(Context ctx) {
		Preferences.setSnqc(ctx, null);
		Preferences.setCodigoSocio(ctx, null);
		Preferences.setUser(ctx, null);
		Preferences.setPassword(ctx, null);
		Preferences.setName(ctx, null);
	}
		
}
