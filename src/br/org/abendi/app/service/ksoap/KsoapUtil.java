package br.org.abendi.app.service.ksoap;

import java.net.Proxy;
import java.util.Date;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.util.Log;
import br.org.abendi.app.util.Constants;

public class KsoapUtil {
	private static final String MAIN_REQUEST_URL = Constants.MAIN_REQUEST_URL_SOAP;
	private static final boolean DEBUG_SOAP_REQUEST_RESPONSE = true;
	
	public static final void testHttpResponse(HttpTransportSE ht) {
		ht.debug = DEBUG_SOAP_REQUEST_RESPONSE;
		if (DEBUG_SOAP_REQUEST_RESPONSE) {
			System.out.println(ht.requestDump);
			Log.v("SOAP RETURN", "Request XML:\n" + ht.requestDump);
			Log.v("SOAP RETURN", "\n\n\nResponse XML:\n" + ht.responseDump);
		}
	}
	
	public static final HttpTransportSE getHttpTransportSE() {
	    HttpTransportSE ht = new HttpTransportSE(Proxy.NO_PROXY,MAIN_REQUEST_URL, 60000);
	    ht.debug = true;
	    ht.setXmlVersionTag("<!--?xml version=\"1.0\" encoding= \"UTF-8\" ?-->");
	    return ht;
	}
	
	public static final SoapSerializationEnvelope getSoapSerializationEnvelope(SoapObject request) {
	    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	    envelope.dotNet = true;
	    envelope.implicitTypes = true;
	    envelope.setAddAdornments(false);
	    envelope.setOutputSoapObject(request);
	    
	    return envelope;
	}
	
	public static final Date formatDate(String dt) {
		dt = dt.replace("/Date(", "");
		dt = dt.substring(0, dt.length() - 7);

		try {
			return (new Date(Long.valueOf(dt)));
		} catch(NumberFormatException e) {
			return null;
		}
	}
}
