package br.org.abendi.app.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NotaExameResult {

	private List<NotaExame> exPraticos;
	private List<NotaExame> exTeoricos;
	private List<NotaExame> exRecertificacao;
	/**
	 * @return the exPraticos
	 */
	public List<NotaExame> getExPraticos() {
		return exPraticos;
	}
	
	/**
	 * @param exPraticos the exPraticos to set
	 */
	public void setExPraticos(NotaExame[] result) {
		if (result == null) {
			return;
		}
		exPraticos = new ArrayList<NotaExame>();
		exPraticos.addAll(Arrays.asList(result));
	}
	
	/**
	 * @param exPraticos the exPraticos to set
	 */
	public void setExPraticos(List<NotaExame> exPraticos) {
		this.exPraticos = exPraticos;
	}
	/**
	 * @return the exTeoricos
	 */
	public List<NotaExame> getExTeoricos() {
		return exTeoricos;
	}
	
	/**
	 * @param exPraticos the exPraticos to set
	 */
	public void setExTeoricos(NotaExame[] result) {
		if (result == null) {
			return;
		}
		exTeoricos = new ArrayList<NotaExame>();
		exTeoricos.addAll(Arrays.asList(result));
	}
	
	/**
	 * @param exTeoricos the exTeoricos to set
	 */
	public void setExTeoricos(List<NotaExame> exTeoricos) {
		this.exTeoricos = exTeoricos;
	}
	/**
	 * @return the exRecertificacao
	 */
	public List<NotaExame> getExRecertificacao() {
		return exRecertificacao;
	}
	
	/**
	 * @param exPraticos the exPraticos to set
	 */
	public void setExRecertificacao(NotaExame[] result) {
		if (result == null) {
			return;
		}
		exRecertificacao = new ArrayList<NotaExame>();
		exRecertificacao.addAll(Arrays.asList(result));
	}
	
	/**
	 * @param exRecertificacao the exRecertificacao to set
	 */
	public void setExRecertificacao(List<NotaExame> exRecertificacao) {
		this.exRecertificacao = exRecertificacao;
	}
	
	
}
