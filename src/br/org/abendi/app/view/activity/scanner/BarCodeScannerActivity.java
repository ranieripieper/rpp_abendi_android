/*
 * Basic no frills app which integrates the ZBar barcode scanner with
 * the camera.
 * 
 * Created by lisah0 on 2012-02-24
 */
package br.org.abendi.app.view.activity.scanner;

import java.io.IOException;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import br.org.abendi.app.R;
import br.org.abendi.app.view.base.BaseActivity;
/* Import ZBar Class files */

public class BarCodeScannerActivity extends BaseActivity {
    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;

    private Button scanButton;
    private View scannerLine;
    
    private ImageScanner scanner;

    private boolean barcodeScanned = false;
    private boolean previewing = true;

    public static final int REQUEST_CODE = 999;
    public static final String RESULT_TEXT_CODE = "RESULT_TEXT_CODE";
    
    static {
        System.loadLibrary("iconv");
    } 

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bar_code_scanner_activity);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();

        /* Instance barcode scanner */
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
        FrameLayout preview = (FrameLayout)findViewById(R.id.cameraPreview);
        preview.addView(mPreview);
        
        scanButton = (Button)findViewById(R.id.ScanButton);
        scannerLine = findViewById(R.id.scanner_line);
        
        scanButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (barcodeScanned) {
                        barcodeScanned = false;
                        mCamera.setPreviewCallback(previewCb);
                        mCamera.startPreview();
                        previewing = true;
                        mCamera.autoFocus(autoFocusCB);
                    }
                }
            });
        
        
        configAnimationScannerLine();
    }
    
    private void configAnimationScannerLine() {
    	/*Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_bottom);
    	anim.setRepeatMode(Animation.INFINITE);
    	scannerLine.startAnimation(anim); */
    	
    	Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_bottom);
    	anim.setRepeatMode(Animation.INFINITE);
    	scannerLine.startAnimation(anim);
    	
    }

    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e){
        }
        return c;
    }
    
    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }
    
    private void reconnectCamera() {
    	
        if (mCamera == null) {
        	mCamera = getCameraInstance();
        	
        }
        previewing = true;
        
        try {
			mCamera.reconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//startPreview();
        //mCamera.setPreviewCallback(previewCb);
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        if (mPreview != null) {
            FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
            preview.removeView(mPreview);
            mPreview = null;
        }

    }

	@Override
	protected void onRestart() {
	    super.onRestart();
	    mCamera = getCameraInstance();    //Open rear camer
	    mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
	    FrameLayout preview = (FrameLayout)findViewById(R.id.cameraPreview);
        preview.addView(mPreview);
	}


    @Override
    protected int hideMenu() {
    	return -1;
    }
    
    @Override
    protected boolean homeAsUpEnabled() {
    	return false;
    }
    
    private Runnable doAutoFocus = new Runnable() {
            public void run() {
                if (previewing)
                    mCamera.autoFocus(autoFocusCB);
            }
        };

    PreviewCallback previewCb = new PreviewCallback() {
            public void onPreviewFrame(byte[] data, Camera camera) {
                Camera.Parameters parameters = camera.getParameters();
                Size size = parameters.getPreviewSize();

                Image barcode = new Image(size.width, size.height, "Y800");
                barcode.setData(data);

                int result = scanner.scanImage(barcode);
                
                if (result != 0) {
                    previewing = false;
                    mCamera.setPreviewCallback(null);
                    mCamera.stopPreview();
                    
                    SymbolSet syms = scanner.getResults();
                    for (Symbol sym : syms) {

                    	Intent intent = new Intent();  
                    	String qrcode = sym.getData();
                    	if (qrcode.indexOf("&id=") > 0) {
                    		qrcode = qrcode.substring(qrcode.indexOf("&id=")+4, qrcode.length());
                    	} else {
                    		qrcode = "";
                    	}
                    	
                    	intent.putExtra(RESULT_TEXT_CODE, qrcode);
                    	setResult(RESULT_OK, intent);
                    	finish();
                        barcodeScanned = true;
                        break;
                    }
                }
            }
        };

    // Mimic continuous auto-focusing
    AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
            public void onAutoFocus(boolean success, Camera camera) {
                autoFocusHandler.postDelayed(doAutoFocus, 1000);
            }
        };
        

}
