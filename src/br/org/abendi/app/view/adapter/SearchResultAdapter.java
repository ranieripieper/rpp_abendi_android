package br.org.abendi.app.view.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import br.org.abendi.app.R;
import br.org.abendi.app.model.Profissional;
import br.org.abendi.app.view.adapter.holder.ViewHolderSearchResult;

import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;

public class SearchResultAdapter extends PagingBaseAdapter<Profissional> {

	private LayoutInflater inflater;
	private Context mContext;
	
	public SearchResultAdapter(Context context, List<Profissional> result) {
		this.mContext = context;
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (result == null) {
			this.items = new ArrayList<Profissional>();
		} else {
			this.items = result;
		}
	}

	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public Profissional getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		
		ViewHolderSearchResult holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.search_result_row, parent, false);
			holder = new ViewHolderSearchResult();
			holder.sqn = (TextView)vi.findViewById(R.id.txt_snqc);
			holder.nome = (TextView)vi.findViewById(R.id.txt_nome);
			holder.dtValidade = (TextView)vi.findViewById(R.id.txt_dt_validade);
			holder.tecnicas = (TextView)vi.findViewById(R.id.txt_tecnicas);
			holder.cidadeEstado = (TextView)vi.findViewById(R.id.txt_cidade_estado);
			
			vi.setTag(holder);
		} else {
			holder = (ViewHolderSearchResult) vi.getTag();
		}

		Profissional profissional = getItem(position);
		holder.sqn.setText(profissional.getSnqc());
		holder.nome.setText(profissional.getNome());
		holder.dtValidade.setText(profissional.getDtValidadeFormat());
		holder.tecnicas.setText(profissional.getTecnicas());
		holder.cidadeEstado.setText(profissional.getCidadeEstado());
		
		return vi;
	}
}