package br.org.abendi.app.model;

import java.util.Date;

import android.text.TextUtils;
import br.org.abendi.app.service.ksoap.KsoapUtil;
import br.org.abendi.app.util.AbendiUtil;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.google.gson.annotations.SerializedName;

public class AgendaExame {

	private String hora;
	private String data;
	@SerializedName("codigo_tecnica")
	private String tecnica;
	private String local;
	/*
[
    {
        "ceq": "CEQ-ABENDE",
        "codigo_tecnica": "AC-N2",
        "data": "/Date(1444014000000-0300)/",
        "hora": "",
        "id": 69017,
        "local": "&lt;a href='http://www.abendi.org.br/abendi/default.aspx?c=343'>clique aqui&lt;/a>",
        "media_etapa1": "",
        "media_etapa2": "",
        "media_etapa3": "",
        "nota1_etapa1": "",
        "nota1_etapa2": "",
        "nota1_etapa3": "",
        "nota2_etapa1": "",
        "nota2_etapa2": "",
        "nota2_etapa3": "",
        "nota_especifica": "",
        "nota_geral": "",
        "numero_snqc": 27193,
        "resultado": ""
    }
]
	 */
	/**
	 * @return the hora
	 */
	public String getHora() {
		return hora;
	}
	/**
	 * @param hora the hora to set
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	/**
	 * @return the data
	 */
	public String getDataFormat() {
		if (data != null) {
			Date dt = KsoapUtil.formatDate(data);
			if (dt != null) {
				return DateUtil.dfDiaMesAno.get().format(dt);
			}			
		}
		return "";		
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}
	/**
	 * @return the tecnica
	 */
	public String getTecnica() {
		return tecnica;
	}
	/**
	 * @param tecnica the tecnica to set
	 */
	public void setTecnica(String tecnica) {
		this.tecnica = tecnica;
	}
	/**
	 * @return the local
	 */
	public String getLocal() {
		return AbendiUtil.getLocal(local);
	}
	/**
	 * @param local the local to set
	 */
	public void setLocal(String local) {
		this.local = local;
	}
	
	
}
