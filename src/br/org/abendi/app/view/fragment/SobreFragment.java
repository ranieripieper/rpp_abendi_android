package br.org.abendi.app.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.org.abendi.app.R;
import br.org.abendi.app.view.base.BaseFragment;

public class SobreFragment extends BaseFragment {

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static SobreFragment newInstance() {
    	SobreFragment fragment = new SobreFragment();
        return fragment;
    }

    public SobreFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sobre_fragment, container, false);
        return rootView;
    }
}