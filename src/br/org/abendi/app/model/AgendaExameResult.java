package br.org.abendi.app.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AgendaExameResult {

	private List<AgendaExame> exPraticos;
	private List<AgendaExame> exTeoricos;
	private List<AgendaExame> exRecertificacao;
	/**
	 * @return the exPraticos
	 */
	public List<AgendaExame> getExPraticos() {
		return exPraticos;
	}
	
	/**
	 * @param exPraticos the exPraticos to set
	 */
	public void setExPraticos(AgendaExame[] result) {
		if (result == null) {
			return;
		}
		exPraticos = new ArrayList<AgendaExame>();
		exPraticos.addAll(Arrays.asList(result));
	}
	
	/**
	 * @param exPraticos the exPraticos to set
	 */
	public void setExPraticos(List<AgendaExame> exPraticos) {
		this.exPraticos = exPraticos;
	}
	/**
	 * @return the exTeoricos
	 */
	public List<AgendaExame> getExTeoricos() {
		return exTeoricos;
	}
	
	/**
	 * @param exPraticos the exPraticos to set
	 */
	public void setExTeoricos(AgendaExame[] result) {
		if (result == null) {
			return;
		}
		exTeoricos = new ArrayList<AgendaExame>();
		exTeoricos.addAll(Arrays.asList(result));
	}
	
	/**
	 * @param exTeoricos the exTeoricos to set
	 */
	public void setExTeoricos(List<AgendaExame> exTeoricos) {
		this.exTeoricos = exTeoricos;
	}
	/**
	 * @return the exRecertificacao
	 */
	public List<AgendaExame> getExRecertificacao() {
		return exRecertificacao;
	}
	
	/**
	 * @param exPraticos the exPraticos to set
	 */
	public void setExRecertificacao(AgendaExame[] result) {
		if (result == null) {
			return;
		}
		exRecertificacao = new ArrayList<AgendaExame>();
		exRecertificacao.addAll(Arrays.asList(result));
	}
	
	/**
	 * @param exRecertificacao the exRecertificacao to set
	 */
	public void setExRecertificacao(List<AgendaExame> exRecertificacao) {
		this.exRecertificacao = exRecertificacao;
	}
	
	
}
