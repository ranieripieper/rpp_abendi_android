package br.org.abendi.app.view.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import br.org.abendi.app.R;
import br.org.abendi.app.service.ksoap.KsoapBaseService;
import br.org.abendi.app.service.ksoap.KsoapServiceBuilder;
import br.org.abendi.app.util.Constants;
import br.org.abendi.app.util.Preferences;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.listeners.ActionClickListener;

public class SelectTecnicasActivity extends Activity {

	public static final String TEXT_RESULT = "TEXT_RESULT";
	
	private ProgressDialog progress;
	protected LoadingView loadingView;
	protected TouchBlackHoleView black_hole;
	protected Snackbar mSnackBar;
	private ListView lstView;
	private ArrayAdapter<String> arrayAdapter;

	private List<String> resultServer = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_tecnicas_activity);
	
		lstView = (ListView)findViewById(R.id.content);
		arrayAdapter = new ArrayAdapter<String>( this, R.layout.simple_list_item, new ArrayList<String>());
		lstView.setAdapter(arrayAdapter); 
		
		lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				String selectItem = (String)arrayAdapter.getItem(position);
				Intent intent = new Intent();  
				intent.putExtra(TEXT_RESULT, selectItem);
            	setResult(RESULT_OK, intent);
            	finish();
			}
		});
		
		loadingView = (LoadingView)findViewById(R.id.progress_bar);
		black_hole = (TouchBlackHoleView)findViewById(R.id.black_hole);
		
		
		//verifica cache
		Date dt = Preferences.getTecnicasLastUpdate(this);
		List<String> lstTecnicas = Preferences.getTecnicas(this);
		
		if (dt == null || lstTecnicas == null || lstTecnicas.size() <= 0 || (int)((new Date().getTime() - dt.getTime())/ (24*60*60*1000)) >= Constants.DIAS_CACHE_TECNICAS) {
        	callService();
        } else {
        	dismissWaitDialog(true);
        	refreshListView(lstTecnicas);
        }		
	}

	private void callService() {
		ObserverAsyncTask<String[]> observer = new ObserverAsyncTask<String[]>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(String[] result) {
				
				if (result != null && result.length > 0) {
					resultServer.addAll(Arrays.asList(result));
					synchronized (arrayAdapter) {
						refreshListView(new ArrayList<String>(resultServer));
					}
					findViewById(R.id.content).setVisibility(View.VISIBLE);
					List<String> list = Arrays.asList(result);
					Preferences.setTecnicas(SelectTecnicasActivity.this, list);
					Preferences.setTecnicasLastUpdate(SelectTecnicasActivity.this, new Date());
				}
				
				dismissWaitDialog(true);
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog(false);
				finish();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		KsoapServiceBuilder sb = new KsoapServiceBuilder();
		sb.setMethodName("SNQCListarTecnicasJSON")
			.setSoapAction("SNQCListarTecnicasJSON")
			.setObserverAsyncTask(observer).setNeedConnection(true);
	
		AsyncTaskService async = sb.mappingInto(this, String[].class, new KsoapBaseService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void refreshListView(List<String> lst) {
		if (arrayAdapter == null) {
			arrayAdapter = new ArrayAdapter<String>( this, R.layout.simple_list_item, lst);
			lstView.setAdapter(arrayAdapter); 
		} else {
			arrayAdapter.clear();
			arrayAdapter.addAll(lst);
			arrayAdapter.notifyDataSetChanged();
		}
		
	}
	
	public void showWaitDialog(boolean blockScreen) {
		if (black_hole != null) {
			if (blockScreen) {
				black_hole.disable_touch(true);
				black_hole.setVisibility(View.VISIBLE);
			} else {
				black_hole.disable_touch(false);
				black_hole.setVisibility(View.GONE);
			}
		}
		
		if (loadingView != null) {
			loadingView.setVisibility(View.VISIBLE);
			loadingView.bringToFront();
		} else {
			progress = ProgressDialog.show(this, getString(R.string.title_aguarde),
					getString(R.string.msg_aguarde), true);
			progress.show();
		}
		if (findViewById(R.id.content) != null) {
			findViewById(R.id.content).setVisibility(View.GONE);
		}
	}
	
	public void showWaitDialog() {
		showWaitDialog(true);
	}
	
	public void dismissWaitDialog(boolean showContent) {
		if (black_hole != null) {
			black_hole.disable_touch(false);
			black_hole.setVisibility(View.GONE);
		}
		if (loadingView != null) {
			loadingView.setVisibility(View.GONE);
		}
		if (progress != null) {
			progress.dismiss();
		}
		
		if (showContent && findViewById(R.id.content) != null) {
			findViewById(R.id.content).setVisibility(View.VISIBLE);
		}
	}
	
	public void showError(Exception e) {
		showError(e, false);
		
	}
	
	public void showError(Exception e, boolean simpleSnack) {
		dismissWaitDialog(false);
		if (e != null) {
			int resMsg = R.string.msg_ocorreu_erro;
			if (e instanceof ConnectionNotFoundException) {
				resMsg = R.string.msg_sem_conexao;
			}
			if (simpleSnack) {
				showSimpleSnack(resMsg);
			} else {
				showSnack(resMsg);
			}
	        
		} else {
			if (simpleSnack) {
				showSimpleSnack(R.string.msg_ocorreu_erro);
			} else {
				showSnack(R.string.msg_ocorreu_erro);
			}
		}
		
	}
	
    protected void showSnack(int msg) {
    	ActionClickListener actionClickListener = new ActionClickListener() {
			
			@Override
			public void onActionClicked(Snackbar snackbar) {
				refresh();
			}
		};
    	
    	mSnackBar = Snackbar.with(this)
                .text(msg)
                .actionLabel(R.string.txt_tentar_novemante)
                .actionListener(actionClickListener)
                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE);
    	 SnackbarManager.show(mSnackBar);
    }
    
    protected void showSimpleSnack(int msg) {
    	showSimpleSnack(msg, Snackbar.SnackbarDuration.LENGTH_SHORT);
    }
    
    protected void showSimpleSnack(int msg, Snackbar.SnackbarDuration duration) {
    	mSnackBar = Snackbar.with(this)
                .text(msg)
                .duration(duration);
    	 SnackbarManager.show(mSnackBar);
    }
      
    protected void refresh() { 
    	if (mSnackBar != null) {
    		mSnackBar.dismiss();
    	} 
    	SnackbarManager.dismiss();
    	callService();
    }
    
}
