package br.org.abendi.app.util;

import android.text.TextUtils;

public class AbendiUtil {

	public static final String getLocal(String local) {
		if (TextUtils.isEmpty(local)) {
			return "";
		} else {
			if (local.indexOf("http://") >= 0 || local.indexOf("https://") >= 0) {
				int posHttp = local.indexOf("http://");
				int finalUrl = local.indexOf("'", posHttp);
				if (finalUrl < 0) {
					finalUrl = local.indexOf("\"", posHttp);
				}
				try {
					return local.substring(posHttp, finalUrl);
				} catch(IndexOutOfBoundsException e) {
					return local;
				}
			} else {
				return local;
			}
		}
	}
}
