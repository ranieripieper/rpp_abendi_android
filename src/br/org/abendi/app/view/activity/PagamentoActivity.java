package br.org.abendi.app.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import br.org.abendi.app.R;
import br.org.abendi.app.model.LoginResult;
import br.org.abendi.app.service.ksoap.KsoapBaseService;
import br.org.abendi.app.service.ksoap.KsoapServiceBuilder;
import br.org.abendi.app.util.Constants;
import br.org.abendi.app.util.Preferences;
import br.org.abendi.app.view.base.BaseActivity;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;

public class PagamentoActivity extends BaseActivity {

	private String tokenPagamento;
	private WebView myWebView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		
		myWebView = (WebView) findViewById(R.id.content);
		WebSettings webSettings = myWebView.getSettings();
		configWebView();
		
		callGetTokenPagamento();
	}
	
	@Override
	protected void refresh() {
		super.refresh();
		callGetTokenPagamento();
	}
	
	private void callGetTokenPagamento() {
    	
    	ObserverAsyncTask<LoginResult> observer = new ObserverAsyncTask<LoginResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(LoginResult result) {
				if (result != null && !TextUtils.isEmpty(result.getCodigoSocio())) {
					tokenPagamento = result.getChaveSeguranca();
				}
				showWebview(tokenPagamento);
			}

			@Override
			public void onCancelled() {
				finish();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		KsoapServiceBuilder sb = new KsoapServiceBuilder();
		sb.setMethodName("LogarJSON")
			.setSoapAction("LogarJSON")
			.setObserverAsyncTask(observer).setNeedConnection(true)
			.addParams("email", Preferences.getUser(getContext()))
			.addParams("senha", Preferences.getPassword(getContext()));
	
		AsyncTaskService async = sb.mappingInto(getContext(), LoginResult.class, new KsoapBaseService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
    }
	
	public void showWebview(String tokenPagamento) {
		String url = String.format(Constants.URL_PAGAMENTO, tokenPagamento);
		myWebView.loadUrl(url);
		myWebView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				dismissWaitDialog(true);
			}
		});

	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    // Check if the key event was the Back button and if there's history
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
	        myWebView.goBack();
	        return true;
	    }
	    // If it wasn't the Back key or there's no web page history, bubble up to the default
	    // system behavior (probably exit the activity)
	    return super.onKeyDown(keyCode, event);
	}
	
	private void configWebView() {
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.getSettings().setUseWideViewPort(true);
		myWebView.getSettings().setLoadWithOverviewMode(true);
		myWebView.getSettings().setPluginState(PluginState.ON);
		myWebView.getSettings().setDomStorageEnabled(true);
		myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);		
	}
	
	public static void showActivity(Context ctx) {
		Intent it = new Intent(ctx, PagamentoActivity.class);
		ctx.startActivity(it);
	}
	
	@Override
	protected int hideMenu() {
		return -1;
	}
	
	@Override
	protected boolean homeAsUpEnabled() {
		return false;
	}
}
