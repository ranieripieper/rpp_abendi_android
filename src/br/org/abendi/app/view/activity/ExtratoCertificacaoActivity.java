package br.org.abendi.app.view.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import br.org.abendi.app.R;
import br.org.abendi.app.model.ExtratoCertificacao;
import br.org.abendi.app.service.ksoap.KsoapBaseService;
import br.org.abendi.app.service.ksoap.KsoapServiceBuilder;
import br.org.abendi.app.util.Preferences;
import br.org.abendi.app.view.adapter.ExtratoCertificaoAdapter;
import br.org.abendi.app.view.base.BaseActivity;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;

public class ExtratoCertificacaoActivity extends BaseActivity {

	private PagingListView lstView;
	private ExtratoCertificaoAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.extrato_certificacoes_activity);
		lstView = (PagingListView)findViewById(R.id.list_view);
		callService();
	}

	private void callService() {
		ObserverAsyncTask<ExtratoCertificacao[]> observer = new ObserverAsyncTask<ExtratoCertificacao[]>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(ExtratoCertificacao[] result) {
				if (result != null && result.length > 0) {
					List<ExtratoCertificacao> lst = new ArrayList<ExtratoCertificacao>();
					lst.addAll(Arrays.asList(result));
					populateView(lst);
				} else {
					showNenhumResultado();
				}
			}

			@Override
			public void onCancelled() {
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		KsoapServiceBuilder sb = new KsoapServiceBuilder();
		sb.setMethodName("SnqcConsultarExtratoJSON")
			.setSoapAction("SnqcConsultarExtratoJSON")
			.setObserverAsyncTask(observer).setNeedConnection(true)
			.addParams("numero_snqc", Preferences.getSnqc(getContext()));
	
		AsyncTaskService async = sb.mappingInto(this, ExtratoCertificacao[].class, new KsoapBaseService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void populateView(List<ExtratoCertificacao> lst) {
		dismissWaitDialog(true);
		if (lst == null || lst.isEmpty()) {
			showNenhumResultado();
		} else {
			
			if (adapter == null) {
				adapter = new ExtratoCertificaoAdapter(getContext(), new ArrayList<ExtratoCertificacao>());
				lstView.setAdapter(adapter);
			}
			//if (Constants.TOTAL_ITENS_PAGE > lst.size()) {
				lstView.onFinishLoading(false, lst);
	    	//} else {
	    	//	lstView.onFinishLoading(true, lst);
	    	//}	
			
		}
	}
	
	private void showNenhumResultado() {
		dismissWaitDialog(false);
		findViewById(R.id.layout_nenhum_resultado).setVisibility(View.VISIBLE);
	}
	
	public static void showActivity(Context ctx) {
		Intent it = new Intent(ctx, ExtratoCertificacaoActivity.class);
		ctx.startActivity(it);
	}
	
	@Override
	protected int hideMenu() {
		return -1;
	}
	
	@Override
	protected boolean homeAsUpEnabled() {
		return false;
	}
}
