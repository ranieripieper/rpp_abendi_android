package br.org.abendi.app.service.ksoap;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;

public class KsoapServiceBuilder extends ServiceBuilder {

	private String methodName = "";
	private String soapAction = "";
	
	public KsoapServiceBuilder setMethodName(String methodName) {
		this.methodName = methodName;
		return this;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @return the soapAction
	 */
	public String getSoapAction() {
		return soapAction;
	}

	/**
	 * @param soapAction the soapAction to set
	 */
	public KsoapServiceBuilder setSoapAction(String soapAction) {
		this.soapAction = soapAction;
		return this;
	}
	
	
	
}
