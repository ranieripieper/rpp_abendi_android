package br.org.abendi.app.view.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import br.org.abendi.app.R;
import br.org.abendi.app.model.ExtratoCertificacao;
import br.org.abendi.app.view.adapter.holder.ViewHolderExtratoCertificacao;

import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;

public class ExtratoCertificaoAdapter extends PagingBaseAdapter<ExtratoCertificacao> {

	private LayoutInflater inflater;
	private Context mContext;
	
	public ExtratoCertificaoAdapter(Context context, List<ExtratoCertificacao> result) {
		this.mContext = context;
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (result == null) {
			this.items = new ArrayList<ExtratoCertificacao>();
		} else {
			this.items = result;
		}
	}

	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public ExtratoCertificacao getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		
		ViewHolderExtratoCertificacao holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.extrato_certificacoes_row, parent, false);
			holder = new ViewHolderExtratoCertificacao();
			holder.tecnica = (TextView)vi.findViewById(R.id.txt_tecnica);
			holder.descricao = (TextView)vi.findViewById(R.id.txt_descricao);
			
			vi.setTag(holder);
		} else {
			holder = (ViewHolderExtratoCertificacao) vi.getTag();
		}

		if (position % 2 == 0) {
			vi.setBackgroundResource(R.color.bg_row_extrato_certificacao_1);
		} else {
			vi.setBackgroundResource(R.color.bg_row_extrato_certificacao_2);
		}
		
		ExtratoCertificacao obj = getItem(position);
		holder.tecnica.setText(obj.getTecnica());
		holder.descricao.setText(obj.getDescricao());
		
		return vi;
	}
}