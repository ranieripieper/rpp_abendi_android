package br.org.abendi.app.util;

public class Constants {
	public static final int DIAS_CACHE_TECNICAS = 1; 
	public static final String URL_PAGAMENTO = "http://www.abendi.org.br/abendi/pgto_app.aspx?token=%s";
	//public static final String URL_PAGAMENTO = "http://www.abendihomologacao.com.br/abendi/pgto_app.aspx?token=%s";
	//public static final String HOST = "http://www.abendi.org.br/abendi_ws/";
	//public static final String MAIN_REQUEST_URL_SOAP = "http://www.abendihomologacao.com.br/abendi_ws/abendi.asmx";
	public static final String MAIN_REQUEST_URL_SOAP = "http://www.abendi.org.br/abendi_ws/abendi.asmx";
	public static final String SOAP_ACTION = "http://tempuri.org/";
	
}
