package br.org.abendi.app.model;

import com.google.gson.annotations.SerializedName;

public class ExtratoCertificacao {

	@SerializedName("codigo_tecnica")
	private String tecnica;
	@SerializedName("status_descricao")
	private String descricao;
	
	/**
	 * @return the tecnica
	 */
	public String getTecnica() {
		return tecnica;
	}
	/**
	 * @param tecnica the tecnica to set
	 */
	public void setTecnica(String tecnica) {
		this.tecnica = tecnica;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
