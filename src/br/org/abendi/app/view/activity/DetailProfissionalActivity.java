package br.org.abendi.app.view.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.org.abendi.app.R;
import br.org.abendi.app.model.Profissional;
import br.org.abendi.app.service.ksoap.KsoapBaseService;
import br.org.abendi.app.service.ksoap.KsoapServiceBuilder;
import br.org.abendi.app.view.base.BaseActivity;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;

public class DetailProfissionalActivity extends BaseActivity {

	private static final String PARAM_TIPO = "PARAM_TIPO";
	private static final String PARAM_VALOR = "PARAM_VALOR";
	private String valor;
	private String tipo;
	
	private TextView txtSnqc;
	private TextView txtNome;
	private TextView txtDtEmissao;
	private LinearLayout layoutRowQualifVig;
	private LayoutInflater inflater;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_profissional_activity);
		
		if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString(PARAM_TIPO) != null) {
			tipo = getIntent().getExtras().getString(PARAM_TIPO);
			valor = getIntent().getExtras().getString(PARAM_VALOR);
			initComponents();
			callService();
		} else {
			finish();
		}	
		
	}
	
	private void initComponents() {
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		txtSnqc = (TextView)findViewById(R.id.txt_snqc);
		txtNome = (TextView)findViewById(R.id.txt_nome);
		txtDtEmissao = (TextView)findViewById(R.id.txt_dt_emissao);
		
		layoutRowQualifVig = (LinearLayout)findViewById(R.id.table_header_rows_qualificacoes_vigentes);
	}
	
	private void callService() {
		ObserverAsyncTask<Profissional[]> observer = new ObserverAsyncTask<Profissional[]>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(Profissional[] result) {
				if (result != null && result.length > 0) {
					List<Profissional> lst = new ArrayList<Profissional>();
					lst.addAll(Arrays.asList(result));
					populateView(lst);
				} else {
					showNenhumResultado();
				}
			}

			@Override
			public void onCancelled() {
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		KsoapServiceBuilder sb = new KsoapServiceBuilder();
		sb.setMethodName("SnqcConsultaPublicaJSON")
			.setSoapAction("SnqcConsultaPublicaJSON")
			.setObserverAsyncTask(observer).setNeedConnection(true)
			.addParams("_tipo", tipo)
			.addParams("_valor", valor);
	
		AsyncTaskService async = sb.mappingInto(this, Profissional[].class, new KsoapBaseService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}		
	}
	
	private void populateView(List<Profissional> profissionais) {
		dismissWaitDialog(true);
		if (profissionais == null || profissionais.isEmpty()) {
			showNenhumResultado();
		} else {
			findViewById(R.id.img_bg).setVisibility(View.GONE);
			Profissional profissional = profissionais.get(0);
			txtNome.setText(profissional.getNome());
			txtSnqc.setText(profissional.getSnqc());
			SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd 'de' MMMM 'de' yyyy ");
			txtDtEmissao.setText(sdf.format(new Date()));
			
			for (int i=0; i < profissionais.size(); i++) {
				addQualificacoesVigentes(profissionais.get(i), i);
			}
		}
	}
	
	private void addQualificacoesVigentes(Profissional prof, int row) {
		View v = inflater.inflate(R.layout.detail_qualificacoes_vigentes_row, null);
		TextView txtTecnica = (TextView)v.findViewById(R.id.txt_tecnica);
		TextView txtDtValidade = (TextView)v.findViewById(R.id.txt_dt_validade);
		txtTecnica.setText(prof.getTecnicas());
		txtDtValidade.setText(prof.getDtValidadeFormat());
		
		if (row % 2 == 0) {
			v.setBackgroundResource(R.color.bg_row_extrato_certificacao_1);
		} else {
			v.setBackgroundResource(R.color.bg_row_extrato_certificacao_2);
		}
		layoutRowQualifVig.addView(v);
	}
	
	private void showNenhumResultado() {
		dismissWaitDialog(false);
		findViewById(R.id.img_bg).setVisibility(View.VISIBLE);
		findViewById(R.id.layout_nenhum_resultado).setVisibility(View.VISIBLE);
	}
	
	@Override
	protected int hideMenu() {
		return -1;
	}
	
	@Override
	protected boolean homeAsUpEnabled() {
		return false;
	}
	
	public static void showActivity(Context ctx, String snqc) {
		Intent it = new Intent(ctx, DetailProfissionalActivity.class);
		String tipo = "";
		String valor = "";
		if (!TextUtils.isEmpty(snqc)) {
			tipo = "snqc";
			valor = snqc;
		}
		it.putExtra(PARAM_TIPO, tipo);
		it.putExtra(PARAM_VALOR, valor);
		ctx.startActivity(it);
	}
}
