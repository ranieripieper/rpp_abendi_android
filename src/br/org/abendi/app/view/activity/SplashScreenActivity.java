package br.org.abendi.app.view.activity;

import android.content.Intent;
import br.org.abendi.app.R;

public class SplashScreenActivity extends com.doisdoissete.android.util.ddsutil.view.SplashScreenActivity {

	
	protected int getContentView() {
		return R.layout.splash_screen_activity;
	}
	
	protected void startMainActivity() {
		startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
	}
	
	/*protected int getSplashScreenTimeOut() {
    	return 1;
    }*/
}
