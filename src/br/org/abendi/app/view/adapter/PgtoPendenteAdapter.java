package br.org.abendi.app.view.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import br.org.abendi.app.R;
import br.org.abendi.app.model.Pagamento;
import br.org.abendi.app.util.Constants;
import br.org.abendi.app.view.adapter.holder.ViewHolderPgtoResult;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;

public class PgtoPendenteAdapter extends PagingBaseAdapter<Pagamento> {

	private LayoutInflater inflater;
	private Context mContext;
	private String tokenPagamento;
	
	public PgtoPendenteAdapter(Context context, List<Pagamento> result, String tokenPgto) {
		this.mContext = context;
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (result == null) {
			this.items = new ArrayList<Pagamento>();
		} else {
			this.items = result;
		}
		tokenPagamento = tokenPgto;
	}

	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public Pagamento getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		
		ViewHolderPgtoResult holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.pagamento_row, parent, false);
			holder = new ViewHolderPgtoResult();
			holder.vencimento = (TextView)vi.findViewById(R.id.txt_vencimento_value);
			holder.valor = (TextView)vi.findViewById(R.id.txt_valor_value);
			holder.descricao = (TextView)vi.findViewById(R.id.txt_desc_value);
			holder.btPgto = (Button)vi.findViewById(R.id.bt_pgar);
			
			vi.setTag(holder);
		} else {
			holder = (ViewHolderPgtoResult) vi.getTag();
		}

		Pagamento pgto = getItem(position);
		holder.vencimento.setText(pgto.getDtVencimentoFormat());
		holder.valor.setText(mContext.getString(R.string.txt_valor_reais, pgto.getValor()));
		holder.descricao.setText(pgto.getDescricao());
		holder.btPgto.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Util.openBrowser(mContext, String.format(Constants.URL_PAGAMENTO, tokenPagamento));
			}
		});
		
		return vi;
	}
}