package br.org.abendi.app.view.adapter.holder;

import android.widget.Button;
import android.widget.TextView;

public class ViewHolderPgtoResult {

	public TextView vencimento;
	public TextView valor;
	public TextView descricao;
	public Button btPgto;
}
