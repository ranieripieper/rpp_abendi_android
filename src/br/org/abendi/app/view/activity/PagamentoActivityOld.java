package br.org.abendi.app.view.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import br.org.abendi.app.R;
import br.org.abendi.app.model.LoginResult;
import br.org.abendi.app.model.Pagamento;
import br.org.abendi.app.service.ksoap.KsoapBaseService;
import br.org.abendi.app.service.ksoap.KsoapServiceBuilder;
import br.org.abendi.app.util.Constants;
import br.org.abendi.app.util.Preferences;
import br.org.abendi.app.view.adapter.PgtoPendenteAdapter;
import br.org.abendi.app.view.base.BaseActivity;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;

public class PagamentoActivityOld extends BaseActivity {

	private PagingListView lstView;
	private PgtoPendenteAdapter adapter;
	private String tokenPagamento;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pagamento_activity);
		lstView = (PagingListView)findViewById(R.id.list_view);
		callGetTokenPagamento();
	}
	
	private void callService() {
		ObserverAsyncTask<Pagamento[]> observer = new ObserverAsyncTask<Pagamento[]>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(Pagamento[] result) {
				if (result != null) {
					List<Pagamento> lst = new ArrayList<Pagamento>();
					lst.addAll(Arrays.asList(result));
					populateView(lst);
				}				
			}

			@Override
			public void onCancelled() {
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		KsoapServiceBuilder sb = new KsoapServiceBuilder();
		sb.setMethodName("CobrancaConsultarBoletoPorUsuarioJSON")
			.setSoapAction("CobrancaConsultarBoletoPorUsuarioJSON")
			.setObserverAsyncTask(observer).setNeedConnection(true)
			.addParams("codigo_snqc", Preferences.getSnqc(getContext()))
			.addParams("codigo_socio", Preferences.getCodigoSocio(getContext()));
	
		AsyncTaskService async = sb.mappingInto(this, Pagamento[].class, new KsoapBaseService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void callGetTokenPagamento() {
    	
    	ObserverAsyncTask<LoginResult> observer = new ObserverAsyncTask<LoginResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(LoginResult result) {
				if (result != null && !TextUtils.isEmpty(result.getCodigoSocio())) {
					tokenPagamento = result.getChaveSeguranca();
				}
				callService();
			}

			@Override
			public void onCancelled() {
			}

			@Override
			public void onError(Exception e) {
				callService();
			}
		};
		
		KsoapServiceBuilder sb = new KsoapServiceBuilder();
		sb.setMethodName("LogarJSON")
			.setSoapAction("LogarJSON")
			.setObserverAsyncTask(observer).setNeedConnection(true)
			.addParams("email", Preferences.getUser(getContext()))
			.addParams("senha", Preferences.getPassword(getContext()));
	
		AsyncTaskService async = sb.mappingInto(getContext(), LoginResult.class, new KsoapBaseService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
    }

	private void populateView(List<Pagamento> pagamentos) {
		dismissWaitDialog(true);
		if (pagamentos == null || pagamentos.isEmpty()) {
			showNenhumResultado();
		} else {
			findViewById(R.id.img_bg).setVisibility(View.GONE);
			if (adapter == null) {
				adapter = new PgtoPendenteAdapter(getContext(), new ArrayList<Pagamento>(), tokenPagamento);
				lstView.setAdapter(adapter);
			}
			
			//if (Constants.TOTAL_ITENS_PAGE > pagamentos.size()) {
			lstView.onFinishLoading(false, pagamentos);
	    	//} else {
	    	//	lstView.onFinishLoading(true, pagamentos);
	    	//}
			
		}
	}
	
	private void showNenhumResultado() {
		dismissWaitDialog(false);
		findViewById(R.id.img_bg).setVisibility(View.VISIBLE);
		findViewById(R.id.layout_nenhum_resultado).setVisibility(View.VISIBLE);
	}
	
	public static void showActivity(Context ctx) {
		Intent it = new Intent(ctx, PagamentoActivityOld.class);
		ctx.startActivity(it);
	}
	
	@Override
	protected int hideMenu() {
		return -1;
	}
	
	@Override
	protected boolean homeAsUpEnabled() {
		return false;
	}
}
