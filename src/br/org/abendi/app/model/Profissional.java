package br.org.abendi.app.model;

import java.util.Date;

import br.org.abendi.app.service.ksoap.KsoapUtil;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.google.gson.annotations.SerializedName;

public class Profissional {

	@SerializedName("numero_snqc")
	private String snqc;
	private String nome;
	@SerializedName("codigo_tecnica")
	private String tecnicas;
	@SerializedName("validade")
	private String dtValidade;
	private String cidade;
	private String estado;
	
	/**
	 * @return the snqc
	 */
	public String getSnqc() {
		return snqc;
	}
	/**
	 * @param snqc the snqc to set
	 */
	public void setSnqc(String snqc) {
		this.snqc = snqc;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the tecnicas
	 */
	public String getTecnicas() {
		return tecnicas;
	}
	/**
	 * @param tecnicas the tecnicas to set
	 */
	public void setTecnicas(String tecnicas) {
		this.tecnicas = tecnicas;
	}
	/**
	 * @return the dtValidade
	 */
	public String getDtValidade() {
		return dtValidade;
	}
	/**
	 * @return the dtValidade
	 */
	public String getDtValidadeFormat() {
		if (dtValidade != null) {
			Date dt = KsoapUtil.formatDate(dtValidade);
			if (dt != null) {
				return DateUtil.dfDiaMesAno.get().format(dt);
			}			
		}
		return "";		
	}
	/**
	 * @param dtValidade the dtValidade to set
	 */
	public void setDtValidade(String dtValidade) {
		this.dtValidade = dtValidade;
	}
	/**
	 * @return the cidadeEstado
	 */
	public String getCidadeEstado() {
		return String.format("%s - %s", cidade, estado);
	}
	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}
	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
