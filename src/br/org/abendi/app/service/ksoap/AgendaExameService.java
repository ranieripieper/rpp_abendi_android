package br.org.abendi.app.service.ksoap;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Context;
import br.org.abendi.app.model.AgendaExame;
import br.org.abendi.app.model.AgendaExameResult;
import br.org.abendi.app.util.Constants;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class AgendaExameService extends BaseService {

	protected static final String NAMESPACE = "http://tempuri.org/";

	private String methodName = "";
	private String soapAction = "";
	
	public AgendaExameService(KsoapServiceBuilder builder) {
		super(builder);
		this.methodName = builder.getMethodName();
		this.soapAction = builder.getSoapAction();
	}

	@Override
	public void refreshServiceBuilder(ServiceBuilder builder) {
		super.refreshServiceBuilder(builder);
		this.methodName = ((KsoapServiceBuilder)builder).getMethodName();
	}

	public AgendaExameResult callService(Context ctx) throws ParseException,
			ConnectionException, ConnectException {
		
		targetType = TypeToken.get(AgendaExame[].class);
		
		AgendaExame[] praticos = callService(ctx, "SnqcConsultarAgendaExamePraticoJSON", Constants.SOAP_ACTION + "SnqcConsultarAgendaExamePraticoJSON");
		AgendaExame[] teoricos = callService(ctx, "SnqcConsultarAgendaExameTeoricoJSON", Constants.SOAP_ACTION + "SnqcConsultarAgendaExameTeoricoJSON");
		AgendaExame[] recertificacao = callService(ctx, "SnqcConsultarAgendaExameRecertificacaoJSON", Constants.SOAP_ACTION + "SnqcConsultarAgendaExameRecertificacaoJSON");
		
		AgendaExameResult res = new AgendaExameResult();
		res.setExPraticos(praticos);
		res.setExTeoricos(teoricos);
		res.setExRecertificacao(recertificacao);
		
		targetType = TypeToken.get(AgendaExameResult.class);
		
		return res;
	}
	
	
	private AgendaExame[] callService(Context ctx, String methodNameParam, String soapActionParam) {
		String data = null;

		SoapObject request = new SoapObject(NAMESPACE, methodNameParam);
		
		if (params != null && params.size() > 0) {
			for (String key : params.keySet()) {
				List<Object> lst = params.get(key);
				if (lst != null && lst.size() > 0) {
					request.addProperty(key, lst.get(0).toString());
				}
			}
		}

		SoapSerializationEnvelope envelope = KsoapUtil
				.getSoapSerializationEnvelope(request);

		HttpTransportSE ht = KsoapUtil.getHttpTransportSE();
		try {
			ht.call(soapActionParam, envelope);
			KsoapUtil.testHttpResponse(ht);
			SoapPrimitive resultsString = (SoapPrimitive) envelope
					.getResponse();

			data = resultsString.toString();
			
			Gson gson = Util.getGson(null, jsonDeserializer, targetType);
			if (data != null) {
				return gson.fromJson(data, targetType.getType());
			}
		} catch (SocketTimeoutException t) {
			t.printStackTrace();
		} catch (IOException i) {
			i.printStackTrace();
		} catch (Exception q) {
			q.printStackTrace();
		}
		return null;
	}
}
