package br.org.abendi.app.view.fragment;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import br.org.abendi.app.R;
import br.org.abendi.app.model.LoginResult;
import br.org.abendi.app.service.ksoap.KsoapBaseService;
import br.org.abendi.app.service.ksoap.KsoapServiceBuilder;
import br.org.abendi.app.util.Preferences;
import br.org.abendi.app.view.activity.AgendamentoActivity;
import br.org.abendi.app.view.activity.ExtratoCertificacaoActivity;
import br.org.abendi.app.view.activity.NotaActivity;
import br.org.abendi.app.view.activity.PagamentoActivity;
import br.org.abendi.app.view.base.BaseFragment;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.EditTextUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;

public class AreaRestritaFragment extends BaseFragment {

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AreaRestritaFragment newInstance() {
    	AreaRestritaFragment fragment = new AreaRestritaFragment();
        return fragment;
    }

    public AreaRestritaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.area_restrtita_fragment, container, false);
        
        return rootView;
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    	super.onViewCreated(view, savedInstanceState);
    	if (TextUtils.isEmpty(Preferences.getSnqc(getActivity()))) {
    		showLogin();
    	} else {
    		showAreaLogada();
    	}    	
    	
    	final EditText txtUsuario = (EditText)getView().findViewById(R.id.edt_usuario);
    	final EditText txtSenha = (EditText)getView().findViewById(R.id.edt_senha);
		
    	txtUsuario.setOnKeyListener(new View.OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
			            (keyCode == KeyEvent.KEYCODE_ENTER)) {
					txtSenha.requestFocus();
				}
				return false;
			}
		});
    	
    	txtSenha.setOnKeyListener(new View.OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
			            (keyCode == KeyEvent.KEYCODE_ENTER)) {
					EditTextUtil.hiddenKeyboard(getActivity(), txtSenha);
					callLogin();
				}
				return false;
			}
		});
    }
    
    private void showLogin() {
    	getView().findViewById(R.id.layout_logado).setVisibility(View.GONE);
    	getView().findViewById(R.id.layout_nao_logado).setVisibility(View.VISIBLE);
 	
    	getView().findViewById(R.id.bt_login).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				callLogin();
			}
		});
    }
    
    private void callLogin() {
    	
    	final EditText txtUsuario = (EditText)getView().findViewById(R.id.edt_usuario);
    	final EditText txtSenha = (EditText)getView().findViewById(R.id.edt_senha);
    	EditTextUtil.hiddenKeyboard(getActivity(), txtUsuario);
    	EditTextUtil.hiddenKeyboard(getActivity(), txtSenha);
    	
    	ObserverAsyncTask<LoginResult> observer = new ObserverAsyncTask<LoginResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(LoginResult result) {
				if (result != null && !TextUtils.isEmpty(result.getNome())) {					
					Preferences.setSnqc(getActivity(), result.getSnqc());
					Preferences.setCodigoSocio(getActivity(), result.getCodigoSocio());
					Preferences.setUser(getActivity(), txtUsuario.getText().toString());
					Preferences.setName(getActivity(), result.getNome());
					Preferences.setPassword(getActivity(), txtSenha.getText().toString());
					showAreaLogada();
				} else {						
					showSimpleSnack(getString(R.string.msg_error_login));
				}
				dismissWaitDialog(true);
			}

			@Override
			public void onCancelled() {
				showContent();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				showContent();
			}
		};
		
		if (TextUtils.isEmpty(txtUsuario.getText()) || TextUtils.isEmpty(txtSenha.getText()) ) {
			showSimpleSnack(getString(R.string.msg_preencha_campos));
		} else {
			KsoapServiceBuilder sb = new KsoapServiceBuilder();
			sb.setMethodName("LogarJSON")
				.setSoapAction("LogarJSON")
				.setObserverAsyncTask(observer).setNeedConnection(true)
				.addParams("email", txtUsuario.getText().toString())
				.addParams("senha", txtSenha.getText().toString());
		
			AsyncTaskService async = sb.mappingInto(getActivity(), LoginResult.class, new KsoapBaseService(sb));
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				async.execute();
			}
		}
    }
    
    @Override
    public void refresh() {
    	super.refresh();
    	callLogin();
    }
    
    private void showAreaLogada() {
    	getView().findViewById(R.id.layout_nao_logado).setVisibility(View.GONE);
    	getView().findViewById(R.id.layout_logado).setVisibility(View.VISIBLE);
    	TextViewPlus txtName = (TextViewPlus)getView().findViewById(R.id.layout_logado).findViewById(R.id.txt_nome_usuario);
    	txtName.setText(Preferences.getName(getActivity()));

    	getView().findViewById(R.id.layout_pgto_pendentes).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PagamentoActivity.showActivity(getActivity());
			}
		});
    	
    	getView().findViewById(R.id.layout_extrato_de_certificacoes).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ExtratoCertificacaoActivity.showActivity(getActivity());
			}
		});
    	
    	getView().findViewById(R.id.layout_agendamento).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AgendamentoActivity.showActivity(getActivity());
			}
		});
    	
    	getView().findViewById(R.id.layout_notas).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				NotaActivity.showActivity(getActivity());
			}
		});

    	getView().findViewById(R.id.bt_sair).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				processLogout();
			}
		});
    }
    
    private void processLogout() {
    	TextView txtUsuario = (TextView)getView().findViewById(R.id.edt_usuario);
		TextView txtSenha = (TextView)getView().findViewById(R.id.edt_senha);
		Preferences.logout(getActivity());
		txtUsuario.setText("");
		txtSenha.setText("");
    	showLogin();
    }
}