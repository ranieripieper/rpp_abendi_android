package br.org.abendi.app.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import br.org.abendi.app.R;
import br.org.abendi.app.view.activity.DetailProfissionalActivity;
import br.org.abendi.app.view.activity.SearchResultActivity;
import br.org.abendi.app.view.activity.SelectTecnicasActivity;
import br.org.abendi.app.view.activity.scanner.BarCodeScannerActivity;
import br.org.abendi.app.view.base.BaseFragment;

import com.doisdoissete.android.util.ddsutil.view.EditTextUtil;

public class BuscaFragment extends BaseFragment {

	private EditText edtSnqc;
	private EditText edtNome;
	private EditText edtTecnica;
	private TextWatcher textWatcherSnqc;
	private TextWatcher textWatcherNome;
	private TextWatcher textWatcherTecnica;
	
	private static final int REQUEST_CODE_TECNICAS = 799;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static BuscaFragment newInstance() {
    	BuscaFragment fragment = new BuscaFragment();
        return fragment;
    }

    public BuscaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.busca_fragment, container, false);
        initComponents(rootView);
        
        return rootView;
    }
    
    private void initComponents(View rootView) {
    	rootView.findViewById(R.id.bt_buscar).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				callSearch();
			}
		});
    	
    	rootView.findViewById(R.id.bt_qr_code).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				openReadQrCode();
			}
		});
    	
    	edtSnqc = (EditText)rootView.findViewById(R.id.edt_nr_snqc);
    	edtNome = (EditText)rootView.findViewById(R.id.edt_nome);
    	edtTecnica = (EditText)rootView.findViewById(R.id.edt_tecnica);
    	
    	TextView.OnEditorActionListener keyListener = new TextView.OnEditorActionListener(){
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                	EditTextUtil.hiddenKeyboard(getActivity(), edtSnqc);
                	EditTextUtil.hiddenKeyboard(getActivity(), edtNome);
                	EditTextUtil.hiddenKeyboard(getActivity(), edtTecnica);
					callSearch();
                }
                return true;
            }
        };
        edtSnqc.setOnEditorActionListener(keyListener);
        edtNome.setOnEditorActionListener(keyListener);
        edtTecnica.setOnEditorActionListener(keyListener);
    	edtSnqc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {

				if (hasFocus) {
					setTextChangedListener();
					edtNome.setText("");
	            	edtTecnica.setText("");
				}
			}
		});
    	
    	edtNome.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {

				if (hasFocus) {
					setTextChangedListener();
					edtSnqc.setText("");
	            	edtTecnica.setText("");
				}
			}
		});
    	
    	edtTecnica.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent it = new Intent(getActivity(), SelectTecnicasActivity.class);
				startActivityForResult(it, REQUEST_CODE_TECNICAS);
			}
		});
    	
    	edtNome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				clearTextChangedListener();
				setTextChangedListener();
			}
		});
    	
    	edtSnqc.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				clearTextChangedListener();
				setTextChangedListener();
			}
		});
    }
    private void clearTextChangedListener() {
    	if (textWatcherSnqc != null) {
    		edtSnqc.removeTextChangedListener(textWatcherSnqc);
    	}
    	if (textWatcherNome != null) {
    		edtNome.removeTextChangedListener(textWatcherNome);
    	}
    }
    
    private void setTextChangedListener() {
    	if (textWatcherSnqc == null) {
    		textWatcherSnqc = new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                }
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                	clearTextChangedListener();
                	edtNome.setText("");
                	edtTecnica.setText("");
                } 
            };
    	}
    	
    	if (textWatcherNome == null) {
    		textWatcherNome = new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                }
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                	clearTextChangedListener();
                	edtSnqc.setText("");
                	edtTecnica.setText("");
                } 
            };
    	}
    	
    	edtNome.addTextChangedListener(textWatcherNome);
    	edtSnqc.addTextChangedListener(textWatcherSnqc);
    }
    
    public void callSearch() {
    	if (TextUtils.isEmpty(edtSnqc.getText()) && TextUtils.isEmpty(edtNome.getText()) 
    			&& TextUtils.isEmpty(edtTecnica.getText()) ) {
    		showSimpleSnack(getString(R.string.msg_preencha_um_dos_campos));
    	} else {
	    	if (TextUtils.isEmpty(edtSnqc.getText().toString())) {
	    		SearchResultActivity.showActivity(getActivity(), edtSnqc.getText().toString(), 
	    				edtNome.getText().toString(), edtTecnica.getText().toString());
	    	} else {
	    		DetailProfissionalActivity.showActivity(getActivity(), edtSnqc.getText().toString());
	    	}
    	}
    	
    }
    
    public void openReadQrCode() {        
    	Intent it = new Intent(getActivity(), BarCodeScannerActivity.class);
    	startActivityForResult(it, BarCodeScannerActivity.REQUEST_CODE);
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	
    	if (requestCode == BarCodeScannerActivity.REQUEST_CODE) {
    		if (resultCode == Activity.RESULT_OK) {
    			if (data != null) {
    				String result = data.getStringExtra(BarCodeScannerActivity.RESULT_TEXT_CODE);
    				edtNome.setText("");
					edtTecnica.setText("");
    				if (!TextUtils.isEmpty(result)) {
    					edtSnqc.setText(result);
    					
    					getView().findViewById(R.id.bt_buscar).requestFocus();
    					callSearch();
    				} else {
    					SearchResultActivity.showActivity(getActivity(), true);
    				}
    			}
    		}
    	} else if (requestCode == REQUEST_CODE_TECNICAS) {
    		if (resultCode == Activity.RESULT_OK) {
    			String result = data.getExtras().getString(SelectTecnicasActivity.TEXT_RESULT);
    			edtSnqc.setText("");
				edtNome.setText("");
				edtTecnica.setText(result);
				getView().findViewById(R.id.bt_buscar).requestFocus();
    		}
    	}
    }

}