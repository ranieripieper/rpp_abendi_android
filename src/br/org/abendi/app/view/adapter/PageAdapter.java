package br.org.abendi.app.view.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import br.org.abendi.app.view.activity.MainActivity;
import br.org.abendi.app.view.base.BaseFragment;
import br.org.abendi.app.view.fragment.AreaRestritaFragment;
import br.org.abendi.app.view.fragment.BuscaFragment;
import br.org.abendi.app.view.fragment.SobreFragment;

public class PageAdapter extends FragmentStatePagerAdapter {
	
	static final int NUM_ITEMS = 3;
	
    public PageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public BaseFragment getItem(int position) {
    	if (position == MainActivity.MENU_SOBRE) {
    		return SobreFragment.newInstance();
    	} else if (position == MainActivity.MENU_AREA_RESTRITA) {
    		return AreaRestritaFragment.newInstance();
    	} else {
    		return BuscaFragment.newInstance();
    	}
    }

}
